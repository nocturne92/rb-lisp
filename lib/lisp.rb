module Lisp
  autoload :Environment, 'lisp/environment'
  autoload :Errors, 'lisp/errors'
  autoload :State, 'lisp/state'
end
