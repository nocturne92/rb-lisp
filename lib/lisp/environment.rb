module Lisp
  class Environment
    attr_reader :table
    def initialize table = {}
      @table = table
    end

    def find name
      @table[name]
    end

    def define name, value
      self.class.new(@table.merge(name => value))
    end

    def apply_namespaces *namespaces
      namespaces.reduce(self) do |env, ns|
        ns.reduce(env) do |env, (k, v)|
          env.define(k, v)
        end
      end
    end
  end
end
