require 'sexpistol'

module Lisp
  class State
    attr_reader :environment
    def initialize environment = Environment.new(globals)
      @environment = environment
    end

    def globals
      {
        :'#t' => true,
        :'#f' => false
      }
    end

    def run exprs
      exprs = case exprs
              when Array
                exprs
              when String
                Sexpistol.new.parse_string(exprs)
              else
                raise ArgumentError
              end

      evaluate(*exprs)
    end

    def with_std
      s, *_ = evaluate(*std)
      s
    end

    def with_env env
      ::Lisp::State.new(env)
    end

    def std
      @__std ||= Sexpistol.new.parse_string(File.read(File.expand_path('../std.scm', __FILE__)))
    end

    def evaluate *expr
      expr.reduce([self, []]) do |(s, rs), expr|
        s, r = case expr
        when Symbol
          [self, s.environment.find(expr)]
        when Array
          case expr[0]
          when :define
            k = expr[1]
            s, (v, *_) = s.evaluate(expr[2])

            [self.class.new(s.environment.define(k, v)), nil]
          when :native
            [self, eval(expr[1])]
          else
            s, (fn, *_) = s.evaluate(expr[0])
            raise Errors::NameNotFound.new(environment, expr[0]) if !fn
            fn[expr[1..-1], s]
          end
        else
          [self, expr]
        end

        [s, [*rs, r]]
      end
    end
  end
end
