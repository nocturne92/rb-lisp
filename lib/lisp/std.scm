(define quote
  (native "-> ((x), s) {
            [s, x]
          }"))

(define unquote
  (native "-> ((q), s) {
            s, (q_,) = s.evaluate(q)
            s, (r,)  = s.evaluate(q_)
            [s, r]
          }"))

(define list
  (native "-> (xs, s) {
            s, rs = s.evaluate(*xs)
            [s, rs]
          }"))

(define if
  (native "-> ((p, a, b), s) {
            s, (p,) = s.evaluate(p)

            if p
              s, (r,) = s.evaluate(a)
              [s, r]
            elsif b
              s, (r,) = s.evaluate(b)
              [s, r]
            end
          }"))
