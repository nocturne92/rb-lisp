module Lisp
  module Errors
    class Base < StandardError
    end

    class NameNotFound < Base
      attr_reader :name, :environment
      def initialize environment, name
        super("symbol not found: #{name}")
        @name = name
        @environment = environment
      end
    end
  end
end
