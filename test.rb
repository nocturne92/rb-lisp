$:.unshift File.expand_path '../lib', __FILE__

require 'lisp'
require 'minitest/autorun'

class TestLisp < MiniTest::Test
  def eval_string str
    Lisp::State.new.with_std.run(str)
  end

  def test_define
      s, rs = eval_string <<-EOF
      (define foo "bar")
      foo
      EOF

      assert_equal(rs, [nil, "bar"])
  end

  def test_list
    s, rs = eval_string <<-EOF
    (list "a" "b" "c")
    EOF

    assert_equal(rs, [["a", "b", "c"]])
  end

  def test_quote
    s, rs = eval_string <<-EOF
    '(a b c)
    '(1 2 3)
    EOF

    assert_equal(rs, [[:a, :b, :c], [1, 2, 3]])
  end

  def test_unquote
    s, rs = eval_string <<-EOF
    (unquote '(list "foo" "bar"))
    EOF

    assert_equal(rs, [["foo", "bar"]])
  end

  def test_if
    s, rs = eval_string <<-EOF
    (if #t
      "foo"
      "bar")
    (if #f
      "bar"
      "baz")
    EOF

    assert_equal(rs, ["foo", "baz"])
  end
end
