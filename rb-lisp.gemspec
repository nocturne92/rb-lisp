Gem::Specification.new do |s|
  s.author = 'Nocturne92'
  s.name = 'rb-lisp'
  s.version = '0.1.0'
  s.summary = s.description = s.name

  s.files = Dir['lib/**/*.rb']

  s.add_runtime_dependency 'hashie', '~> 3.3'
  s.add_runtime_dependency 'sexpistol', '~> 0.0.7'

  s.add_development_dependency 'pry', '~> 0.10'
end
